<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Password Wajib Diisi</name>
   <tag></tag>
   <elementGuidId>497bcbf1-119c-4eaf-8be2-6cc7752072da</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='form']/div/div/div[4]/div/div/div[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.alert.alert-danger > div:nth-of-type(2)</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>25bf4bf2-04c3-4b91-a53e-9cbbf18e782b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Password Wajib Diisi</value>
      <webElementGuid>73f78eaa-4f0d-4509-9709-bf410ecfd411</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;form&quot;)/div[@class=&quot;h-100 d-flex justify-content-center align-items-center&quot;]/div[@class=&quot;pb-4 pt-4&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-12&quot;]/div[@class=&quot;alert alert-danger&quot;]/div[2]</value>
      <webElementGuid>78bd16d6-98d0-46cd-8471-ba3d3e890104</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='form']/div/div/div[4]/div/div/div[2]</value>
      <webElementGuid>a70bda12-c5cb-47de-8b6e-b065d2209712</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Email Wajib Diisi'])[1]/following::div[1]</value>
      <webElementGuid>df794b39-642e-4c6d-98da-b5209163f696</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lupa Password?'])[1]/following::div[5]</value>
      <webElementGuid>ac261e00-a812-44c1-bdd8-d47153e373aa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Daftar'])[1]/preceding::div[1]</value>
      <webElementGuid>b8c5f2fc-cbea-4a79-a9d7-c7b1c2557881</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Masuk'])[2]/preceding::div[1]</value>
      <webElementGuid>8864c077-3637-4985-b9df-493f345ec9ce</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Password Wajib Diisi']/parent::*</value>
      <webElementGuid>66de42ba-d24b-4279-8f89-4ebfec45fb9e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/div/div[2]</value>
      <webElementGuid>f6a59a30-e3df-4caf-aef2-4c81d8c2ad11</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Password Wajib Diisi' or . = 'Password Wajib Diisi')]</value>
      <webElementGuid>35e26ad1-1f6f-4962-bb70-65af4d251fbd</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
