<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Email Wajib Diisi</name>
   <tag></tag>
   <elementGuidId>781df7b9-6af4-4757-8c0b-b12adbfaf3bb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='form']/div/div/div[4]/div/div/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.alert.alert-danger > div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>a907f059-619a-4228-85ff-aeea8301f3bd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Email Wajib Diisi</value>
      <webElementGuid>d076b774-8286-4b2f-bf5e-a8e5a5549623</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;form&quot;)/div[@class=&quot;h-100 d-flex justify-content-center align-items-center&quot;]/div[@class=&quot;pb-4 pt-4&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-12&quot;]/div[@class=&quot;alert alert-danger&quot;]/div[1]</value>
      <webElementGuid>9fe89e0b-b52e-40ff-89e5-8812cfe62f72</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='form']/div/div/div[4]/div/div/div</value>
      <webElementGuid>0b678d0f-79a3-44e8-97fe-c2dfe052faa2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lupa Password?'])[1]/following::div[4]</value>
      <webElementGuid>021806bf-9589-454c-8bcf-7f3a61468adc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[2]/following::div[7]</value>
      <webElementGuid>e5132e40-9083-4e05-abdf-535995305d58</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Password Wajib Diisi'])[1]/preceding::div[1]</value>
      <webElementGuid>9326607e-dd7b-4135-ac4a-55f10f19769f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Daftar'])[1]/preceding::div[2]</value>
      <webElementGuid>28711bf7-a411-4685-91bd-a5dcbb33d8d9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Email Wajib Diisi']/parent::*</value>
      <webElementGuid>50c9e2f8-d501-4f65-8c26-8b3df5146739</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/div/div</value>
      <webElementGuid>3f566504-a935-45c2-893d-4f1bd7f7819e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Email Wajib Diisi' or . = 'Email Wajib Diisi')]</value>
      <webElementGuid>07f3fbcd-c8b1-408a-a8f8-bf318011bfc9</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
