<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>i_Tentang Kami_fa fa-instagram</name>
   <tag></tag>
   <elementGuidId>91642531-419f-435d-bcf1-ebaa3688125f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div/div/div/div/div/a/i</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a.rounded-circle.bg-light > i.fa.fa-instagram</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>i</value>
      <webElementGuid>858b2882-ac2c-4ac0-aa34-ff5da9899d50</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fa fa-instagram</value>
      <webElementGuid>36c5b9a7-8cec-4d54-87cc-e89fb1a787db</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;fixed-top&quot;]/header[@class=&quot;header shadow&quot;]/div[@class=&quot;header__top&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-lg-12&quot;]/div[@class=&quot;header__top__right&quot;]/div[@class=&quot;header__top__right__social&quot;]/a[@class=&quot;rounded-circle bg-light&quot;]/i[@class=&quot;fa fa-instagram&quot;]</value>
      <webElementGuid>5d685d7e-8612-4e27-a63a-5ab6c4f6ccff</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div/div/div/a/i</value>
      <webElementGuid>aa5aea66-9059-4ace-a432-f3b74704ee9c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
